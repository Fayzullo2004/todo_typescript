import styles from './home.module.css'
import {ChangeEvent, useState} from "react";
import {IData} from "./interface";
import {data} from "./constants";

const App = (): JSX.Element => {

  const [title, setTitle] = useState<string>()
  const [arr, setArr] = useState<IData[]>(data)

  const changeHandler = (evt: ChangeEvent<HTMLInputElement>): void => {
    setTitle(evt.target.value)
  }

  const handleSubmit = (): void => {
    if (!title?.length) return;
    const newData = {
      title: title,
      id: new Date().getTime(),
      description: 'Description'
    }
    setArr([...arr, newData]);
    setTitle('')
  }

  const deleteItem = (id: number): void => {
    const newData = arr.filter(c => c.id != id)
    setArr(newData)
  }


  return (
    <div className={styles.todo}>
      <h1 className={styles.title}>APP TODO</h1>
      <input placeholder='Enter todo' className={styles.input} value={title} onChange={changeHandler}/>
      <button className={styles.button} onClick={handleSubmit}>Add Todo</button>

      <div className={styles.card}>
        {arr.map(c => (
          <div key={c.id} className={styles.cardItem}>
            <p>{c.title}</p>
            <div className={styles.delBtn}>
              <button onClick={() => deleteItem(c.id)}>Del</button>
            </div>
          </div>
        ))}
      </div>

    </div>
  )
}

export default App